using System.Linq;
namespace MVCDemoSoft2.Repository
{
    public interface IRepository<T>
    {
        IQueryable<T> Table{get;}
        void Insert(T entity);

        void Update(T entity);

        void Delete(int id);
    }
}