﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MVCDemoSoft2.Migrations
{
    public partial class 增加公共属性 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedTime",
                table: "BookLibrary",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "DisplayOrder",
                table: "BookLibrary",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsActived",
                table: "BookLibrary",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "BookLibrary",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Remarks",
                table: "BookLibrary",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedTime",
                table: "BookLibrary",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedTime",
                table: "Book",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "DisplayOrder",
                table: "Book",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsActived",
                table: "Book",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Book",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Remarks",
                table: "Book",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedTime",
                table: "Book",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedTime",
                table: "BookLibrary");

            migrationBuilder.DropColumn(
                name: "DisplayOrder",
                table: "BookLibrary");

            migrationBuilder.DropColumn(
                name: "IsActived",
                table: "BookLibrary");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "BookLibrary");

            migrationBuilder.DropColumn(
                name: "Remarks",
                table: "BookLibrary");

            migrationBuilder.DropColumn(
                name: "UpdatedTime",
                table: "BookLibrary");

            migrationBuilder.DropColumn(
                name: "CreatedTime",
                table: "Book");

            migrationBuilder.DropColumn(
                name: "DisplayOrder",
                table: "Book");

            migrationBuilder.DropColumn(
                name: "IsActived",
                table: "Book");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Book");

            migrationBuilder.DropColumn(
                name: "Remarks",
                table: "Book");

            migrationBuilder.DropColumn(
                name: "UpdatedTime",
                table: "Book");
        }
    }
}
