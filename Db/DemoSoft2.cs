using Microsoft.EntityFrameworkCore;
using MVCDemoSoft2.Models;

namespace MVCDemoSoft2.Db
{
    public class DemoSoft2 : DbContext
    {
        string connectString = "server=.;database=DemoSoft2;uid=sa;pwd=123456;";
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {

            builder.UseSqlServer(connectString);
        }



        public DbSet<BookLibrary> BookLibrary { get; set; }
        public DbSet<Book> Book { get; set; }
    }
}