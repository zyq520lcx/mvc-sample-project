namespace MVCDemoSoft2.Models
{
    public class Book:BaseEntity
    {
        
        public string BookName { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public decimal Price { get; set; }
        public string ISBN { get; set; }


        public int BookLibraryId { get; set; }
        public BookLibrary BookLibrary { get; set; }
    }
}